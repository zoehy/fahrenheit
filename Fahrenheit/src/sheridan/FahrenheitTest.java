package sheridan;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class FahrenheitTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testConvertFromCelsius() {
		assertEquals(68, Fahrenheit.convertFromCelsius(20));
	}
	
	@Test
	public void testConvertFromCelsiusBoundaryIn() {
		assertEquals(32, Fahrenheit.convertFromCelsius(0));
	}
	
	@Test
	public void testConvertFromCelsiusBoundaryOut() {
		assertNotEquals(72, Fahrenheit.convertFromCelsius(22));
	}
	
	@Test
	public void testConvertFromCelsiusException() {
		assertNotEquals(-2189.2, Fahrenheit.convertFromCelsius(-1234));
	}

}
