package sheridan;

public class Fahrenheit {
	public static int convertFromCelsius(int c) {
		int fahrenheit = (int) ((9.0/5.0)*c + 32);
		
		return fahrenheit;
	}
	
	public static void main(String[] args) {
		System.out.println(convertFromCelsius(22));
	}
}
